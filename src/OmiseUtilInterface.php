<?php

namespace Drupal\commerce_omise;

use Drupal\commerce_price\Price;

/**
 * The Omise utilities service interface.
 */
interface OmiseUtilInterface {

  /**
   * Formats the charge amount for Omise.
   *
   * @param \Drupal\commerce_price\Price $amount
   *   The amount being charged.
   *
   * @return int
   *   The Omise formatted amount.
   */
  public function formatNumber(Price $amount);

  /**
   * Maps the Omise credit card type to a Commerce credit card type.
   *
   * @param string $cardType
   *   The Omise credit card type.
   *
   * @return string
   *   The Commerce credit card type.
   */
  public function mapCreditCardType($cardType);

  /**
   * Translates Omise exceptions into Commerce exceptions.
   *
   * @param \OmiseException $exception
   *   The Omise exception.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   The Commerce exception.
   */
  public function handleException(\OmiseException $exception);

}
