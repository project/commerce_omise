<?php

namespace Drupal\commerce_omise;

use Drupal\commerce_payment\Exception\AuthenticationException;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\Exception\InvalidRequestException;
use Drupal\commerce_payment\Exception\InvalidResponseException;
use Drupal\commerce_price\Price;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * The Omise utilities.
 */
class OmiseUtil implements OmiseUtilInterface {

  /**
   * The commerce_payment entity type storage.
   *
   * @var \Drupal\commerce_payment\PaymentStorageInterface
   */
  protected $paymentStorage;

  /**
   * The commerce_payment_gateway config entity type storage.
   *
   * @var \Drupal\commerce_payment\PaymentGatewayStorageInterface
   */
  protected $paymentGatewayStorage;

  /**
   * Constructs new OmiseUtil object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->paymentStorage = $entityTypeManager->getStorage('commerce_payment');
    $this->paymentGatewayStorage = $entityTypeManager->getStorage('commerce_payment_gateway');
  }

  /**
   * {@inheritdoc}
   */
  public function formatNumber(Price $amount) {
    $amount_number = $amount->getNumber();

    switch ($amount->getCurrencyCode()) {
      case 'THB':
      case 'SGD':
      case 'USD':
      case 'EUR':
      case 'GBP':
        $amount_number = $amount_number * 100;
        break;

      default:
        break;
    }

    return number_format($amount_number, 0, '.', '');
  }

  /**
   * {@inheritdoc}
   */
  public function mapCreditCardType($cardType) {
    $map = [
      'American Express' => 'amex',
      'Diners Club' => 'dinersclub',
      'JCB' => 'jcb',
      'MasterCard' => 'mastercard',
      'Visa' => 'visa',
    ];
    if (!isset($map[$cardType])) {
      throw new HardDeclineException(sprintf('Unsupported credit card type "%s".', $cardType));
    }

    return $map[$cardType];
  }

  /**
   * {@inheritdoc}
   */
  public function handleException(\OmiseException $exception) {
    if ($exception instanceof \OmiseInvalidCardException) {
      throw new DeclineException('We encountered an error processing your card details. Please verify your details and try again.');
    }
    elseif ($exception instanceof \OmiseBadRequestException) {
      throw new InvalidRequestException('Invalid parameters were supplied to Omise\'s API.');
    }
    elseif ($exception instanceof \OmiseAuthenticationFailureException) {
      throw new AuthenticationException('Omise authentication failed.');
    }
    else {
      throw new InvalidResponseException($exception->getMessage());
    }
  }

}
