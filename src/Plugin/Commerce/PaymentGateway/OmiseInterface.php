<?php

namespace Drupal\commerce_omise\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsRefundsInterface;

/**
 * Provides the interface for the Omise payment gateway.
 */
interface OmiseInterface extends
    OnsitePaymentGatewayInterface,
    SupportsAuthorizationsInterface,
    SupportsRefundsInterface,
    OffsitePaymentGatewayInterface {

  const OMISE_REMOTE_STATUS_AWAITING_OTP_CAPTURE = 'awaiting_otp_for_capture';
  const OMISE_REMOTE_STATUS_AWAITING_OTP_AUTHORIZE = 'awaiting_otp_for_authorize';
  const OMISE_REMOTE_STATUS_CAPTURED = 'captured';
  const OMISE_REMOTE_STATUS_AUTHORIZED = 'authorized';
  const OMISE_REMOTE_STATUS_VOIDED = 'voided';
  const OMISE_REMOTE_STATUS_REFUNDED = 'refunded';
  const OMISE_REMOTE_STATUS_PARTIALLY_REFUNDED = 'partially_refunded';

  /**
   * Get the Omise API Public key set for the payment gateway.
   *
   * @return string
   *   The Omise API public key.
   */
  public function getPublicKey();

  /**
   * Retrieve and update payment status for an existing charge.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   The payment.
   *
   * @throws \Drupal\commerce_payment\Exception\PaymentGatewayException
   *   The Commerce exception.
   */
  public function updateRemotePaymentStatus(PaymentInterface $payment);

}
