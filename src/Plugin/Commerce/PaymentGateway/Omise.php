<?php

namespace Drupal\commerce_omise\Plugin\Commerce\PaymentGateway;

use Drupal\commerce\Response\NeedsRedirectException;
use Drupal\commerce_omise\OmiseUtilInterface;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides the Omise payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "omise",
 *   label = "Omise (CreditCard)",
 *   display_label = "Omise",
 *   forms = {
 *     "add-payment-method" = "Drupal\commerce_omise\PluginForm\Omise\PaymentMethodAddForm",
 *   },
 *   payment_method_types = {"credit_card"},
 *   credit_card_types = {
 *     "amex", "dinersclub", "jcb", "mastercard", "visa",
 *   },
 *   js_library = "commerce_omise/form",
 *   payment_type = "payment_omise",
 *   requires_billing_information = FALSE
 * )
 */
class Omise extends OnsitePaymentGatewayBase implements OmiseInterface {

  /**
   * Omise utils.
   *
   * @var \Drupal\commerce_omise\OmiseUtilInterface
   */
  protected OmiseUtilInterface $util;

  /**
   * Constructs new Omise object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time.
   * @param \Drupal\commerce_price\MinorUnitsConverterInterface $minor_units_converter
   *   The minor units converter.
   * @param \Drupal\commerce_omise\OmiseUtilInterface $util
   *   Omise utils.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    PaymentTypeManager $payment_type_manager,
    PaymentMethodTypeManager $payment_method_type_manager,
    TimeInterface $time,
    MinorUnitsConverterInterface $minor_units_converter,
    OmiseUtilInterface $util
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);
    $this->util = $util;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('commerce_price.minor_units_converter'),
      $container->get('commerce_omise.util')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPublicKey() {
    return $this->configuration['public_key'];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'secret_key' => '',
      'public_key' => '',
      '3d_secure' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['public_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public key'),
      '#default_value' => $this->configuration['public_key'],
      '#required' => TRUE,
    ];

    $form['secret_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Secret key'),
      '#default_value' => $this->configuration['secret_key'],
      '#required' => TRUE,
    ];

    $form['3d_secure'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable 3D Secure support'),
      '#description' => $this->t('Required if 3D Secure is enabled for your Omise account. Note that this would introduce redirecting user to Omise and back to your site during checkout.'),
      '#default_value' => !empty($this->configuration['3d_secure']),
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['public_key'] = $values['public_key'];
      $this->configuration['secret_key'] = $values['secret_key'];
      $this->configuration['3d_secure'] = $values['3d_secure'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $transaction_data = [
      'currency' => $payment->getAmount()->getCurrencyCode(),
      'amount' => $this->util->formatNumber($payment->getAmount()),
      'capture' => $capture,
      // Always passing in a card since the user have selected or entered the
      // specific card in Drupal user interface.
      // Passing the 'customer' param without a card would always force using
      // users default card stored at Omise regardless of what card has been
      // selected in Drupal.
      'card' => $payment_method->getRemoteId(),
    ];

    $owner = $payment_method->getOwner();
    if ($owner
      && $owner->isAuthenticated()
      && $remoteCustomer = $this->getRemoteOmiseCustomer($owner)) {
      // Add remote customer ID if it's available; this is required for
      // stored cards.
      // @see doCreatePaymentMethod()
      $transaction_data['customer'] = $remoteCustomer['id'];
    }

    if (!empty($this->configuration['3d_secure'])) {
      if ($payment->isNew()) {
        // Save the payment entity to get its ID in case if it's not available
        // yet.
        $payment->save();
      }

      $transaction_data['return_uri'] = Url::fromRoute('commerce_payment.checkout.return', [
        'commerce_order' => $payment->getOrderId(),
        'step' => 'payment',
      ], [
        'absolute' => TRUE,
        'query' => [
          // @see onReturn()
          'payment_id' => $payment->id(),
        ],
      ])->toString();
    }

    try {
      $result = \OmiseCharge::create(
        $transaction_data,
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
      $this->validateChargeStatus($result);
    }
    catch (\OmiseException $e) {
      $this->util->handleException($e);
    }

    $payment->setRemoteId($result['id']);
    if (!empty($result['authorize_uri'])) {
      // Payment gateway requested redirecting user to their payment
      // authorization page (e.g. for 3D Secure check).
      // Store the payment (so the payment remote ID gets stored) and redirect
      // user.
      $payment->setRemoteState($capture
        ? self::OMISE_REMOTE_STATUS_AWAITING_OTP_CAPTURE
        : self::OMISE_REMOTE_STATUS_AWAITING_OTP_AUTHORIZE
      );
      $payment->save();
      throw new NeedsRedirectException($result['authorize_uri']);
    }

    $payment->setRemoteState($capture
      ? self::OMISE_REMOTE_STATUS_CAPTURED
      : self::OMISE_REMOTE_STATUS_AUTHORIZED
    );
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization', 'manual_capture']);
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    try {
      $charge = \OmiseCharge::retrieve(
        $payment->getRemoteId(),
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
      $charge->capture();
    }
    catch (\OmiseException $e) {
      $this->util->handleException($e);
    }

    $payment->setState('completed');
    $payment->setRemoteState(self::OMISE_REMOTE_STATUS_CAPTURED);
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    $this->assertPaymentState($payment, ['authorization', 'manual_capture']);
    $amount = $payment->getAmount();
    // Void Omise payment - release uncaptured payment.
    try {
      $remote_id = $payment->getRemoteId();
      $charge = \OmiseCharge::retrieve(
        $remote_id,
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
      $data = [
        'amount' => $this->util->formatNumber($amount),
        'void' => TRUE,
      ];

      $charge->refunds()->create($data);
    }
    catch (\OmiseException $e) {
      $this->util->handleException($e);
    }

    // Update payment.
    $payment->setState('authorization_voided');
    $payment->setRemoteState(self::OMISE_REMOTE_STATUS_VOIDED);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['completed', 'partially_refunded']);
    // If not specified, refund the entire amount.
    $amount = $amount ?: $payment->getAmount();
    // Validate the requested amount.
    $this->assertRefundAmount($payment, $amount);

    try {
      $remote_id = $payment->getRemoteId();
      $data = [
        'amount' => $this->util->formatNumber($amount),
      ];
      $charge = \OmiseCharge::retrieve(
        $remote_id,
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
      $charge->refunds()->create($data);
    }
    catch (\OmiseException $e) {
      $this->util->handleException($e);
    }

    $old_refunded_amount = $payment->getRefundedAmount();
    $new_refunded_amount = $old_refunded_amount->add($amount);
    if ($new_refunded_amount->lessThan($payment->getAmount())) {
      $payment->setState('partially_refunded');
      $payment->setRemoteState(self::OMISE_REMOTE_STATUS_PARTIALLY_REFUNDED);
    }
    else {
      $payment->setState('refunded');
      $payment->setRemoteState(self::OMISE_REMOTE_STATUS_REFUNDED);
    }

    $payment->setRefundedAmount($new_refunded_amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      // The expected keys are payment gateway specific and usually match
      // the PaymentMethodAddForm form elements. They are expected to be valid.
      'omise_token',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    $remote_payment_method = $this->doCreatePaymentMethod($payment_method, $payment_details);
    $payment_method->card_type = $this->util->mapCreditCardType($remote_payment_method['brand']);
    $payment_method->card_number = $remote_payment_method['last_digits'];
    $payment_method->card_exp_month = $remote_payment_method['expiration_month'];
    $payment_method->card_exp_year = $remote_payment_method['expiration_year'];
    $expires = CreditCard::calculateExpirationTimestamp($remote_payment_method['expiration_month'], $remote_payment_method['expiration_year']);
    $payment_method->setRemoteId($remote_payment_method['id']);
    if (isset($remote_payment_method['token'])) {
      // Handle one-time / non-reusable card.
      $payment_method->setRemoteId($remote_payment_method['token']);
      $payment_method->setReusable(FALSE);
    }
    $payment_method->setExpiresTime($expires);
    $payment_method->save();
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    // Delete the remote record.
    try {
      $owner = $payment_method->getOwner();
      if ($owner
        && $customer = $this->getRemoteOmiseCustomer($owner)) {
        $customer->cards()->retrieve($payment_method->getRemoteId())->destroy();
      }
    }
    catch (\OmiseException $e) {
      $this->util->handleException($e);
    }

    $payment_method->delete();
  }

  /**
   * {@inheritdoc}
   */
  public function updateRemotePaymentStatus(PaymentInterface $payment) {
    $this->assertPaymentState($payment, [
      'new',
      'authorization',
      'manual_capture',
    ]);

    try {
      $charge = \OmiseCharge::retrieve(
        $payment->getRemoteId(),
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
      $this->validateChargeStatus($charge);
    }
    catch (\OmiseException $e) {
      // Remove payment method from the order.
      // Note we are NOT removing the payment method; it's still valid (since
      // it passed the initial card number check), it's just not usable for this
      // order (most likely due to invalid 3D Secure response provided by user).
      // Removing the order payment method would force user re-entering (or
      // selecting) it again.
      $order = $payment->getOrder();
      $order->get('payment_method')->setValue(NULL);
      $order->save();

      $this->util->handleException($e);
    }

    if (
      !empty($payment_state = $this->getPaymentStateByRemote($payment->getRemoteState(), $charge['status'] ?? NULL))
    ) {
      $payment->setState($payment_state);
      $payment->setRemoteState($payment_state == 'completed'
        ? self::OMISE_REMOTE_STATUS_CAPTURED
        : self::OMISE_REMOTE_STATUS_AUTHORIZED
      );
    }
    $payment->save();
  }

  /**
   * Gets payment internal state by remote state.
   *
   * @param string|null $remote_state
   *   Remote payment state.
   * @param string|null $charge_status
   *   Charge status.
   *
   * @return string|null
   *   Returns payment internal state or NULL if remote state is not defined.
   */
  protected function getPaymentStateByRemote(?string $remote_state, ?string $charge_status) {
    switch ($remote_state) {
      case self::OMISE_REMOTE_STATUS_AWAITING_OTP_AUTHORIZE:
      case self::OMISE_REMOTE_STATUS_AUTHORIZED:
        return 'authorization';

      case self::OMISE_REMOTE_STATUS_AWAITING_OTP_CAPTURE:
      case self::OMISE_REMOTE_STATUS_CAPTURED:
        if ($charge_status && $charge_status == 'pending') {
          return 'authorization';
        }
        return 'completed';

      default:
        return NULL;
    }
  }

  /**
   * Validates Omise charge status.
   *
   * @param \OmiseCharge $charge
   *   The Omise charge object.
   *
   * @throws \OmiseException
   *   Omise errors are thrown if charge failed.
   */
  protected function validateChargeStatus(\OmiseCharge $charge) {
    if (!empty($charge['status'] && $charge['status'] === 'failed')) {
      throw \OmiseException::getInstance([
        'code' => $charge['failure_code'],
        'message' => $charge['failure_message'],
      ]);
    }
  }

  /**
   * Creates the payment method on the gateway.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method
   *   The payment method.
   * @param array $payment_details
   *   The gateway-specific payment details.
   *
   * @return array
   *   The payment method information returned by the gateway. Notable keys:
   *   - id: The remote card ID. This is a remote REUSABLE card object ID which
   *     should be only used for authorized users.
   *   - token: The ONE TIME card token.
   *   Credit card specific keys:
   *   - card_type: The card type.
   *   - last4: The last 4 digits of the credit card number.
   *   - expiration_month: The expiration month.
   *   - expiration_year: The expiration year.
   */
  protected function doCreatePaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {

    $owner = $payment_method->getOwner();
    $customer = NULL;
    if ($owner && $owner->isAuthenticated()) {
      $customer = $this->getRemoteOmiseCustomer($owner);
    }
    if ($customer) {
      // If the customer already exists
      // use the Omise form token to create the new card.
      // Retrieve card ID by token. It will be later used to identify the newly
      // created card in the customer's cards list.
      $cardByToken = $this->retrieveCardIdByToken($payment_details['omise_token']);

      // Create a payment method for an existing customer.
      $customer->update(['card' => $payment_details['omise_token']]);

      // Retrieve the list of cards, starting with the newest first.
      $cards = \OmiseCustomer::retrieve(
        $customer['id'],
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      )
        ->cards(['order' => 'reverse_chronological']);

      // Verify card was successfully added to the customer's account.
      $newlyCreatedCards = array_filter($cards['data'], function ($card) use ($cardByToken) {
        return $card['id'] === $cardByToken;
      });
      if (count($newlyCreatedCards) !== 1) {
        throw new \RuntimeException('Could not retrieve newly created Omise card.');
      }

      return reset($newlyCreatedCards);
    }
    elseif ($owner && $owner->isAuthenticated()) {
      // Create both the customer and the payment method.
      try {
        $customer = \OmiseCustomer::create([
          'email' => $owner->getEmail(),
          'description' => $this->t('Customer for :mail', [':mail' => $owner->getEmail()]),
          'card' => $payment_details['omise_token'],
        ],
          $this->configuration['public_key'],
          $this->configuration['secret_key']
        );

        $cards = \OmiseCustomer::retrieve(
          $customer['id'],
          $this->configuration['public_key'],
          $this->configuration['secret_key']
        )->cards();

        $this->setRemoteCustomerId($owner, $customer['id']);
        $owner->save();

        if (count($cards['data']) !== 1) {
          throw new \RuntimeException(sprintf('Expected customer to have only 1 card, %d found.', count($cards['data'])));
        }

        return reset($cards['data']);
      }
      catch (\OmiseException $e) {
        $this->util->handleException($e);
      }
    }
    else {
      $existingCard = \OmiseToken::retrieve(
        $payment_details['omise_token'],
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
      $cardByToken = $existingCard['card'];
      $cardByToken['token'] = $payment_details['omise_token'];
      return $cardByToken;
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getNotifyUrl() {
    // This is a copy of OffsitePaymentGatewayBase::getNotifyUrl().
    // Omise doesn't support usual Notify (i.e. when notify URL is passed in as
    // part of the API call), however, there's a similar thing called Webhooks
    // so keeping this code here for future reference.
    return Url::fromRoute('commerce_payment.notify', [
      'commerce_payment_gateway' => $this->parentEntity->id(),
    ], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $payment_id = $request->query->get('payment_id');
    if ($payment_id === NULL || !is_numeric($payment_id)) {
      throw new \InvalidArgumentException('Missing/invalid payment ID query parameter.');
    }

    $payment = $this
      ->entityTypeManager
      ->getStorage('commerce_payment')
      ->load($payment_id);
    if (!$payment) {
      throw new \RuntimeException('The payment with given ID does not exist.');
    }

    $this->updateRemotePaymentStatus($payment);
  }

  /**
   * {@inheritdoc}
   */
  public function onCancel(OrderInterface $order, Request $request) {
    $this->messenger()->addMessage($this->t('You have canceled checkout at @gateway but may resume the checkout process here when you are ready.', [
      '@gateway' => $this->getDisplayLabel(),
    ]));
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $content = $request->getContent() ? Json::decode($request->getContent()) : [];
    if (
      empty($content)
      || empty($remoteId = $content['data']['id'])
      || empty($status = $content['data']['status'])
    ) {
      return new Response('', 401);
    }

    $payment = $this->entityTypeManager
      ->getStorage('commerce_payment')
      ->loadByRemoteId($remoteId);

    if (!$payment instanceof PaymentInterface) {
      return new Response('', 404);
    }

    if ($payment->getState()->getId() != 'manual_capture') {
      $payment->setState($this->getStateByPaymentStatus($status));
      $payment->save();
    }

    return NULL;
  }

  /**
   * Gets the payment state depends on charge status.
   *
   * @param string $status
   *   Charge status.
   *   According to the Omise API we can receive one of those: failed, expired,
   *   pending, reversed or successful.
   *
   * @see https://www.omise.co/charges-api
   *
   * @return string
   *   Returns payment state.
   */
  protected function getStateByPaymentStatus(string $status) {
    switch ($status) {
      case 'successful':
        return 'completed';

      case 'failed':
        return 'authorization_voided';

      case 'expired':
        return 'authorization_expired';

      case 'pending':
        return 'authorization';

      case 'reversed':
        return 'refunded';
    }
  }

  /**
   * Retrieves card ID by token.
   *
   * @param string $token
   *   The token string.
   *
   * @return string
   *   The card ID.
   */
  protected function retrieveCardIdByToken($token) {
    $cardByToken = \OmiseToken::retrieve(
      $token,
      $this->configuration['public_key'],
      $this->configuration['secret_key']
    );
    if (empty($cardByToken['card']['id'])) {
      throw new \RuntimeException('Could not retrieve card ID using token.');
    }

    return $cardByToken['card']['id'];
  }

  /**
   * Retrieves remote Omise customer object.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account.
   *
   * @return \OmiseCustomer|false
   *   Omise customer object or FALSE if not found.
   */
  protected function getRemoteOmiseCustomer(UserInterface $account) {
    $customerId = $this->getRemoteCustomerId($account);
    if (!$customerId) {
      return FALSE;
    }

    try {
      return \OmiseCustomer::retrieve(
        $customerId,
        $this->configuration['public_key'],
        $this->configuration['secret_key']
      );
    }
    catch (\OmiseNotFoundException $e) {
      if (!$account->isAnonymous()) {
        $this->setRemoteCustomerId($account, NULL);
        $account->save();
      }
      return FALSE;
    }
  }

}
