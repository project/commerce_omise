<?php

namespace Drupal\commerce_omise\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the Omise payment type.
 *
 * @CommercePaymentType(
 *   id = "payment_omise",
 *   label = @Translation("Omise"),
 *   workflow = "payment_omise",
 * )
 */
class PaymentOmise extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
